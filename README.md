
<!-- README.md is generated from README.Rmd. Please edit that file -->

# florencemounier

<!-- badges: start -->
<!-- badges: end -->

The goal of florencemounier is to pass the N2 exam

## Installation

You can install the development version of florencemounier like so:

-   From .tar.gz file:

``` r
remotes::install_local(path = 'florencemounier_0.0.2.tar.gz')
```

-   From gitlab:

``` r
remotes::install_git("https://gitlab.com/florence7/florencemounier.git")
```

## How to use this package

``` r
browseURL(system.file("site", "index.html", package = "florencemounier"))
```
